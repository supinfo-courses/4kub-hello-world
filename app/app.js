const os = require('os')
const twig = require('twig')
const express = require('express')
const app = express()
const redis = require('redis')
let redisClient = redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
})

redisClient.on('error', function (err) {
    console.log('Redis error -> ' + err)
})

app.set('twig options', {
    allow_async: true,
    strict_variables: false
})

app.get('/', function (req, res) {
    // CPU usage simulation.
    for (let i=0; i<1e7; i++);

    res.render('./index.twig', {
        hostname: os.hostname(),
        redisClient: redisClient,
        adminPassword: process.env.APPLICATION_ADMIN_PASSWORD
    })
})

app.listen(3000, function () {
    console.log('App/Server is launched...')
})
